package fernandez.pau.shoppinglist;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by pablofd on 13/03/2018.
 */

public class ShoppingItem {
    private String text;
    private boolean checked;
    private Date date = new Date();

    public ShoppingItem(String text, boolean checked) { //quan crea va a aquí
        this.text = text;
        this.checked = checked;

    }

    public ShoppingItem(String text) {
        this.text = text;
        this.checked = false;

        // Obtenir un instant de temps
         //date = new Date(); // agafa l'instant actual del dispositiu


    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void toggleChecked() {
        this.checked = !this.checked;
    }

    public Date getDate() {
        return date;
    }


}
