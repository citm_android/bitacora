package fernandez.pau.shoppinglist;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by pablofd on 06/03/2018.
 */

public class ShoppingListAdapter extends ArrayAdapter<ShoppingItem> {
    //private Context c;




    public ShoppingListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<ShoppingItem> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // 1. Crear un nou View si és necessari (no cal si convertView no és null)
        View root = convertView; // arrel d'un item de la llista
        if (root == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            root = inflater.inflate(R.layout.shopping_item, parent, false);
        }
        final TextView edit_text =  root.findViewById(R.id.title_item);
        TextView fecha = root.findViewById(R.id.fecha);
        TextView datetime = root.findViewById(R.id.datetime);
        final ShoppingItem item = getItem(position);
        edit_text.setText(item.getText());


        Date date = new Date();

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(item.getDate());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min  = calendar.get(Calendar.MINUTE);
        fecha.setText(String.format("%02d/%02d/%04d", day, month+1, year));
        datetime.setText(String.format("%02d:%02d",hour+1, min+1));
        //checkBox.setChecked(item.isChecked());

        return root;
    }





}
